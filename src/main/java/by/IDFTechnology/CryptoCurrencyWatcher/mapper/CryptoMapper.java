package by.IDFTechnology.CryptoCurrencyWatcher.mapper;

import by.IDFTechnology.CryptoCurrencyWatcher.dto.crypto.CryptoCreateDto;
import by.IDFTechnology.CryptoCurrencyWatcher.dto.crypto.CryptoFullDto;
import by.IDFTechnology.CryptoCurrencyWatcher.entity.CryptoEntity;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface CryptoMapper {

    CryptoFullDto map(CryptoEntity entity);

    List<CryptoFullDto> map(List<CryptoEntity> entities);

    CryptoEntity map(CryptoCreateDto dto);

}
