package by.IDFTechnology.CryptoCurrencyWatcher.mapper;

import by.IDFTechnology.CryptoCurrencyWatcher.dto.user.UserCreateDto;
import by.IDFTechnology.CryptoCurrencyWatcher.dto.user.UserFullDto;
import by.IDFTechnology.CryptoCurrencyWatcher.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CryptoMapper.class})
public interface UserMapper {

    UserFullDto map(UserEntity entity);

    List<UserFullDto> map(List<UserEntity> entities);

    UserEntity map(UserCreateDto dto);

}
