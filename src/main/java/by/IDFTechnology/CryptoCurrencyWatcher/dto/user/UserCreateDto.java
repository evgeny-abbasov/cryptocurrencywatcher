package by.IDFTechnology.CryptoCurrencyWatcher.dto.user;

import lombok.Data;

@Data
public class UserCreateDto {

    private String name;
}
