package by.IDFTechnology.CryptoCurrencyWatcher.dto.user;

import by.IDFTechnology.CryptoCurrencyWatcher.dto.crypto.CryptoFullDto;

import lombok.Data;

import java.util.List;

@Data
public class UserFullDto {

    private Integer id;
    private String name;
    private List<CryptoFullDto> cryptos;
}
