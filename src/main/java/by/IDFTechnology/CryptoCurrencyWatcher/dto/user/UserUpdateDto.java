package by.IDFTechnology.CryptoCurrencyWatcher.dto.user;

import lombok.Data;

@Data
public class UserUpdateDto {

    private Integer id;
    private String name;

}
