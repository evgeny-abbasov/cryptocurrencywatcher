package by.IDFTechnology.CryptoCurrencyWatcher.dto.crypto;

import lombok.Data;

@Data
public class CryptoUpdateDto {

    private Integer id;
    private String crypto_id;
    private String symbol;
    private String price;
    private String percent;
}
