package by.IDFTechnology.CryptoCurrencyWatcher.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "crypto")
public class CryptoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String crypto_id;

    @Column
    private String symbol;

    @Column
    private String price;

    @Column
    private String percent;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;


}
