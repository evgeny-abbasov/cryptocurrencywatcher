package by.IDFTechnology.CryptoCurrencyWatcher.repository;

import by.IDFTechnology.CryptoCurrencyWatcher.entity.CryptoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CryptoRepository extends JpaRepository<CryptoEntity, Integer> {
}
