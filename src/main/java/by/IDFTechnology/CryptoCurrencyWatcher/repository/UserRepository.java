package by.IDFTechnology.CryptoCurrencyWatcher.repository;

import by.IDFTechnology.CryptoCurrencyWatcher.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {
}
